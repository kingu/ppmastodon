from mastodon import Mastodon
import sys, os, time, re, shlex, subprocess, json, tagpy, random, plyer, shelve
from datetime import datetime

now = datetime.now() # current date and time

poll_length = 1500 
logfile = "/tmp/ppmastodon.log"
keepfile = "/tmp/{}.m3u".format(now.strftime("%m-%d-%Y"))
dbfileApp = "/home/jphil/Documents/dev/ppmastodon/ppmastodon-app.db"
dbfileSel = "/home/jphil/Documents/dev/ppmastodon/ppmastodon-sel.db"

def log(stuff):
    with open(logfile, "a") as myfile:
        myfile.write(str(stuff) + "\n")

def keep(stuff):
    with open(keepfile, "a") as myfile:
        myfile.write(str(stuff) + "\n")
        
def _getallfiles(musicdir):
    # Return a list of audio files in {musicdir}
    af = subprocess.check_output(['find', musicdir]).splitlines()
    res = []
    for bf in af:
        try:
            f = bf.decode()
            if os.path.splitext(f)[1] in [ '.mp3', '.ogg', '.flac' ]:
                res.append(f)
        except Exception as e:
            log("Bad utf-8 encoding: " + str(bf), "errors")
            continue
    log (len(res))
    return res

def getallfiles(musicdir, rules):
    log(rules)
    if not rules:
        return _getallfiles(musicdir)
    try:
        return list(map(lambda x: x.decode(), subprocess.check_output(['/home/jphil/bin/playmusic', '-m', musicdir, '-l', '4'] + rules).splitlines()))
    except Exception as e:
        log(e)
        return False
    
def post_this_track(fn, reply_to=None):
    
    if os.path.isfile(fn):
        filesize = os.path.getsize(fn)
        log("Posting file! [{}] {}".format(filesize,fn))
        ext = os.path.splitext(fn)[1]
        if filesize > 40000000:
            log("FILE TOO BIG!!!\nConverting to mp3")
            sub=subprocess.run(["ffmpeg", "-y", "-i",  fn, "/tmp/out.mp3"])
            log(sub)
            return post_this_track("/tmp/out.mp3", reply_to=reply_to)
        fref = tagpy.FileRef(fn)
        tagref = fref.tag()

        mime = { ".mp3":"audio/mpeg", ".ogg":"audio/ogg", ".flac":"audio/flac" }[ext]
        message = "Artist: {artist}\nTitle: {title}\nYear: {year}\nAlbum: {album}\nGenre(s): {genre}".format(
            artist=getattr(tagref, "artist", "--empty--")[0:150],
            title=getattr(tagref, "title", "--empty--")[0:150],
            year=getattr(tagref, "year", "????"),
            album=getattr(tagref, "album", "--empty--")[0:150],
            genre=getattr(tagref, "genre", "--empty--")
        )
        log(message)
        af = mastodon.media_post(fn, mime_type=mime, description=None, focus=None)
        return mastodon.status_post(message, media_ids=af, visibility='unlisted', in_reply_to_id=reply_to)
    else:
        log("File is invalid: {}".format(fn))
        return False

def makePoll(tracklist):
    options = []
    db = shelve.open(dbfileApp)
    for track in tracklist:
        log(track)
        fref = tagpy.FileRef(track)
        tagref = fref.tag()
        print(type(tagref.artist))
        options.append("{} - {}".format(tagref.artist, tagref.title)[0:50])
    return mastodon.make_poll(options, poll_length, multiple=False, hide_totals=False)
    
        
def  ToKen (token):
    text_file = open(".{}_token".format(token), "r")
    data = text_file.read()
    text_file.close()
    return data

themes = { "J-Rock & Metal":"J-Rock,Japanese Metal",
           "J-Pop":"J-Pop",
           "Heavy Metal":"Heavy Metal",
           "Jazz & Blues":"Jazz,Blues",
           "Folk":"Folk",
           "Noise":"Noise",
           "Rock":"Rock",
           "Electronic":"Electronic Music,EDM",
           "Ska & Reggae":"Reggae,Ska",
           "Punk":"Punk Rock",
           "Doom":"Doom Metal",
           "Psychedelic":"Psychedelia",
           "Neo Psychedelia":"Neo Psychedelia",
           "Prog":"Progressive Rock",
           "Psychedelic & Prog Rock":"Progressive Rock,Psychedelic Rock",
           "Vocaloid":"Vocaloid",
           "New Waves and Post Punk":"New Wave",
           "Pop":"Pop",
           "Electro Pop":"Electro Pop",
           "Folk Metal":"Folk Metal",
           
           "Chaos - no theme":False}

def main(arguments):
    log(datetime.now())
    notifs = mastodon.notifications()
    for n in notifs:
        if n["type"] == "mention":
            log(n["status"]["tags"])
    selectedTheme=random.choice(list(themes.keys()))
    if themes[selectedTheme]:
        allfiles = getallfiles("/media/jphil/Music/", rules=['-ig', themes[selectedTheme]])
    else:
        allfiles = getallfiles("/media/jphil/Music/", False)
    log(selectedTheme)
    if len(allfiles) > 4:
        tracklist = [random.choice(allfiles), random.choice(allfiles), random.choice(allfiles), random.choice(allfiles)]
    else:
        tracklist = allfiles
    pollstatus = mastodon.status_post("🎺 What to play next?\nRandom theme: {}\n".format(selectedTheme), poll=makePoll(tracklist), visibility='public')
    pollid = pollstatus["poll"]["id"]
    thePoll = mastodon.poll(pollid)
    while not thePoll["expired"]:
        time.sleep(100)
        thePoll = mastodon.poll(pollid)
        # log(thePoll)
    thePoll = mastodon.poll(pollid)
    str = ""
    for x in thePoll['options']:
        str +=  "{} {}\n".format(x["votes_count"], x["title"])        
    log("Poll completed.  Voters count is: {}\n{}".format(thePoll['voters_count'],str))
    if thePoll['votes_count']  == 0:
        log("No votes, no tracks!")
        mastodon.status_delete(pollstatus)
        return False
    numlist = list(map(lambda x: x["votes_count"], thePoll["options"]))
    track = tracklist[numlist.index(max(numlist))]
    
    try:
        pt = post_this_track (track, pollstatus)
        log(pt)            
        keep(track)
    except Exception as e:
        log(e)

if __name__ == '__main__':
    random.seed()
    mastodon = Mastodon(access_token = ToKen("mastodon"), api_base_url = "https://botsin.space")
    sys.exit(main(sys.argv[1:]))


