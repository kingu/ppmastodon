from mastodon import Mastodon, StreamListener
import sys, os, re, shlex, subprocess, json, tagpy, random, plyer

def getallfiles(musicdir):
    # Return a list of audio files in {musicdir}
    af = subprocess.check_output(['find', musicdir]).splitlines()
    res = []
    for bf in af:
        try:
            f = bf.decode()
            if os.path.splitext(f)[1] in [ '.mp3', '.ogg', '.flac' ]:
                res.append(f)
        except Exception as e:
            log("Bad utf-8 encoding: " + str(bf), "errors")
            continue            
    return res

def post_this_track(fn):
    print(fn)
    if os.path.isfile(fn):
        fref = tagpy.FileRef(fn)
        tagref = fref.tag()
        message = "Artist: {artist}\nTitle: {title}\nYear: {year}\nAlbum: {album}\nGenre(s): {genre}".format(
            artist=getattr(tagref, "artist", "--empty--"),
            title=getattr(tagref, "title", "--empty--"),
            year=getattr(tagref, "year", "????"),
            album=getattr(tagref, "album", "--empty--"),
            genre=getattr(tagref, "genre", "--empty--")
        )
        print(message)
        af = mastodon.media_post(fn, mime_type=None, description=None, focus=None)
        mastodon.status_post(message, media_ids=af, visibility='unlisted')
        plyer.notification.notify(title="Pouet Pouet!!!", message="{artist} - {title}".format(artist=getattr(tagref, "artist", "--empty--"),
                                                                                              title=getattr(tagref, "title", "--empty--")))


        
def  ToKen (token):
    text_file = open(".{}_token".format(token), "r")
    data = text_file.read()
    text_file.close()
    return data

def main(arguments):
    allfiles = getallfiles("/media/jphil/Music/")
    post_this_track (random.choice(allfiles))

if __name__ == '__main__':
    random.seed()
    mastodon = Mastodon(access_token = ToKen("mastodon"), api_base_url = "https://botsin.space")
    sys.exit(main(sys.argv[1:]))


