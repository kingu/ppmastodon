from mastodon import Mastodon, StreamListener
import sys, os, re, shlex, subprocess, json, tagpy

ALLOW = [ "kingu_platypus_gidora@octodon.social" ]

class Bot(StreamListener):

    def __init__(self):
        super(Bot, self).__init__()

    def on_notification(self, notification):
        if notification["type"] == "mention":
            if notification["status"]["account"]["acct"] in ALLOW:
                proceed(notification["status"])
            else:
                send_access_denied_notice(notification["status"])

                
# def proceed(status):
#     command = re.sub('<[^<]+?>', '', status['content']).replace("@platypus","").strip()
#     cmd = parse_command(command)
#     if cmd:
#         send_confirmation(status)
#         result = execute_command(cmd)
#         if result:
#             send_completed_confirmation(status, result)
#         else:
#             send_failure_notice(status)
#     else:
#         send_syntax_error_notice(status)

def proceed(status):
    command = re.sub('<[^<]+?>', '', status['content']).replace("@platypus","").strip()
    cmd=shlex.split(command)
    if cmd[0] == "get":
        return getfile(cmd[1], status)    

def getfile(f, status):
    fn = "/media/jphil/Music{}".format(f)
    print(fn)
    if os.path.isfile(fn):
        af = mastodon.media_post(fn, mime_type=None, description=None, focus=None)
        mastodon.status_reply(status, "Here it is\n", media_ids=af)
        print ("It's been tooted")
        
def makeAPoll():
    pass
        
def parse_command(command):
    try:
        cmd = shlex.split(command)
        title = cmd.pop(0)
        if title == "poll":
            return [title]
        inc = []
        exc = []
        while not cmd[0] == "-" :
            inc.append(cmd.pop(0))
        if cmd:        
            cmd.pop(0)
            while cmd:
                exc.append(cmd.pop(0))
        return [ title, inc, exc]
    except:
        return False
    
def tootIt(status, message):
    if status:
        mastodon.status_reply(status, message)
    else:
        mastodon.toot(message)
        
def execute_command(cmd):
    print(cmd)
    mycom =["/home/jphil/bin/playmusic",
            "-m", "/media/jphil/Music/",
            "-t", cmd[0],
            "-ig", ",".join(cmd[1]),
            "-xg", ",".join(cmd[2]),
            "-l", "5" ]
    print(mycom)
    if not subprocess.run(mycom):
        return False
    
    mycom =["/home/jphil/bin/platypus",
            "/tmp/playmusic.m3u",
            "-t", cmd[0],
            "--video",
            "-vtbi", "/home/jphil/Documents/dev/ppmastodon/platypus-text-bg.png",
            "-vtf", "{artist} - [{year}] {title} ({genre})",
            "-mc", "/home/jphil/Documents/dev/ppmastodon/nocover.png"
    ]
    print (mycom)
    if not subprocess.run(mycom):
        return False
    secrets = readPeertubeSecrets(".peertube_secrets")
    mycom = ["/home/jphil/.local/bin/ptu",
             "-e", "https://{}".format(secrets["server"]),
             "upload-video", "-c", "platypus", "-n", cmd[0], "-t", getPeertubeToken(secrets), "/home/jphil/Videos/{}.mp4".format(cmd[0])]
    print(mycom)
    out = subprocess.run(mycom, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if not out.returncode == 0:
        print ("RC:",type(out.returncode))
        return False
    result = out.stderr.decode().splitlines()[1]

    return result
    
def validate_command(cmd):
    return true

def send_confirmation(status):
    return tootIt(status, "Your command was received and will now be executed\nBe patient, it may takes a while.")

def send_completed_confirmation(to, result):
    return tootIt(to, "Your command ran successfully!! \n\n{}".format(result))

def send_failure_notice(to):
    return tootIt(to, "Sorry, an error occurs while running your command.")

def send_command_error_notice(to):
    return tootIt(to, "Sorry. I cannot understand your command :(")

def send_syntax_error_notice(to):
    return tootIt(to, "Sorry, you are not allowed to use this app.  Please contact @kingu_platypus_gidora@octodon.social")

def readPeertubeSecrets(secretsfile):
    text_file = open(secretsfile, "r")
    data = text_file.read()
    text_file.close()
    sbl = data.splitlines()
    res = {}
    for l in sbl:
        spl=l.split("=")
        res[spl[0]] = spl[1]
    return res

def getPeertubeToken(data):
    print (data)
    res = subprocess.run(['curl', '-X', 'POST', "-d",
                         "client_id={client_id}&client_secret={client_secret}&grant_type=password&response_type=code&username={username}&password={password}".format(**data),
                          "https://{server}/api/v1/users/token".format(**data)], stdout=subprocess.PIPE)
    return json.loads(res.stdout.decode())["access_token"] 
                                    
def  ToKen (token):
    text_file = open(".{}_token".format(token), "r")
    data = text_file.read()
    text_file.close()
    return data

if __name__ == '__main__':
    mastodon = Mastodon(access_token = ToKen("mastodon"), api_base_url = "https://botsin.space")
    mastodon.stream_user(Bot())
    sys.exit(main(sys.argv[1:]))


